import React from "react";
import Button from "../../components/Button/Button";
// history.pushを使うと、SPAでも戻る・進むボタン使えるよ
const Home = ({ history }) => {
  return (
    <div>
      <h1>HOME</h1>
      <Button onClickHandler={() => history.push("/quiz")}>Quiz Page</Button>
      <Button
        onClickHandler={() => {
          console.log("ここにAPIを出せるようにしたいね")
        }}
      >
        API
      </Button>
    </div>
  );
};

export default Home;
