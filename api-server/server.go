package main

// 参考 : https://qiita.com/aki36-an/items/bb25af254c5566328960
import (
	"net/http"

	"github.com/labstack/echo"
)

func main() {
	e := echo.New()
	routing(e)
	e.Logger.Fatal(e.Start(":1323"))
}

/* ルーティングを行う */
func routing(e *echo.Echo) {
	e.GET("/", hello)
	e.GET("/:name", greeting)
}

/* http:/~/ の時 */
func hello(c echo.Context) error {
	return c.JSON(http.StatusOK, map[string]string{"message": "hello"})
}

/* http:/~/(名前) の時 */
func greeting(c echo.Context) error {
	/* c.Param("name")　とすることで、URLの:name　と対応させて取得 */
	return c.JSON(http.StatusOK, map[string]string{"message": "hello " + c.Param("name")})
}
